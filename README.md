Mali RSS skupljač koji ima sposobnosti:

* unosa feedova putem "New Feed" gumba pri vrhu stranice
* izlistavanja članaka po pojedinom feedu
* mala tražilica za traženje članaka po piscima/autorima
* preproznavanje ako feed već postoji tako da ne bi unjeli isti feed dva puta

Potrebne knjižnice:

* Django==1.9.7
* feedparser==5.2.1

Izdanje Pythona:

* Python== 3.4.2
____

U slučaju nekih problema mogu staviti i svoj virtualenv u ovaj spremnik. Probleme s kodom slobodno javite na mail: "stipebalenovic@gmail.com"