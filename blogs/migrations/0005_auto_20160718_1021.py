# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-18 10:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0004_auto_20160718_1010'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feed',
            name='name',
        ),
        migrations.AlterField(
            model_name='article',
            name='author',
            field=models.CharField(blank=True, default='Unknown', max_length=300),
        ),
    ]
