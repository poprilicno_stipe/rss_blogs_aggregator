from django.db import models

# Create your models here.


class Feed(models.Model):
    title = models.CharField(max_length=200)
    url = models.URLField()
    active = models.BooleanField(default=False)
    name = models.CharField(max_length=200, default="")

    def __str__(self):
        return self.title


class Article(models.Model):
    feed = models.ForeignKey(Feed)
    title = models.CharField(max_length=300)
    url = models.URLField()
    description = models.TextField()
    author = models.CharField(max_length=300, default="Unknown Author")
    publication_date = models.DateTimeField()

    def __str__(self):
        return self.title
