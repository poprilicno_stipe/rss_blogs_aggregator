from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^new_feed', views.new_feed, name='new_feed'),
    url(r'^feed_selected/(?P<feed_selected>\d+)/$', views.feed_selected, name='feed_selected'),
    url(r'^', views.articles_list, name='articles_list'),
]

