from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Article, Feed
from .forms import FeedForm

import feedparser
import datetime

# Create your views here.


def articles_list(request):
    # listing all articles
    entries_list = Article.objects.all().order_by('-publication_date')
    # listing all feeds
    feeds = Feed.objects.all()
    # queries for searching via search field
    query = request.GET.get('q')
    if query:
        entries_list = entries_list.filter(author__contains=query)
    paginator = Paginator(entries_list, 20)  # Show 20 contacts per page
    page = request.GET.get('page')
    try:
        entries = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        entries = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        entries = paginator.page(paginator.num_pages)
    return render(request, 'blogs/articles_list.html', {'entries': entries, 'feeds': feeds})


def feed_selected(request, feed_selected):
    # selecting feed
    feed_id_selection = Feed.objects.filter(id=feed_selected)
    # selecting articles form previously selected feed
    feed_articles = Article.objects.filter(feed=feed_id_selection).order_by('-publication_date')
    query = request.GET.get('q')
    if query:
        feed_articles = feed_articles.filter(author__contains=query)
    paginator = Paginator(feed_articles, 20)  # Show 20 contacts per page
    page = request.GET.get('page')
    try:
        entries = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        entries = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        entries = paginator.page(paginator.num_pages)
    return render(request, 'blogs/feed_selected.html', {'entries': entries})


def new_feed(request):
    if request.method == 'POST':
        form = FeedForm(request.POST)
        if form.is_valid():
            feed = form.save(commit=False)
            existing_feed = Feed.objects.filter(url=feed.url)
            if len(existing_feed) == 0:
                feed_data = feedparser.parse(feed.url)
                feed.title = feed_data.feed.title
                feed.save()
                for entry in feed_data.entries:
                    article = Article()
                    article.title = entry.title
                    # checking if author attribute exists
                    while True:
                        try:
                            article.author = entry.author
                            break
                        except AttributeError:
                            article.author = "Unknown Author"
                            break
                    article.url = entry.link
                    article.description = entry.description
                    # parsing publication date
                    d = datetime.datetime(*(entry.published_parsed[0:6]))
                    date_string = d.strftime('%Y-%m-%d %H:%M:%S')
                    article.publication_date = date_string
                    article.feed = feed
                    article.save()
            return redirect('blogs/views.articles_list')
    else:
        form = FeedForm()
    return render(request, 'blogs/new_feed.html', {'form': form})


